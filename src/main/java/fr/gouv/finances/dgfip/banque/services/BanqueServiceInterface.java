package fr.gouv.finances.dgfip.banque.services;

import fr.gouv.finances.dgfip.banque.entities.Banque;
import fr.gouv.finances.dgfip.banque.entities.CarteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteCourant;
import fr.gouv.finances.dgfip.banque.entities.CompteEpargne;
import fr.gouv.finances.dgfip.banque.entities.Personne;

public interface BanqueServiceInterface {
    public Banque creerBanque(String nomBanque);

    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
	    String guichet, Double soldeInitial);

    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
	    String guichet);

    public CompteEpargne creerCompteEpargne(Banque banque, Personne titulaire,
	    String guichet, Double soldeinitial, Double taux);

    public CarteBancaire creerCarte(Banque banque, Personne titulaire,
	    CompteCourant compte);

    public void afficherSyntheseComptes(Banque banque);

    public void afficherSyntheseCartes(Banque banque);
}
