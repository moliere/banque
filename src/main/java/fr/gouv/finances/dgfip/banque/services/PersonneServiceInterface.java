package fr.gouv.finances.dgfip.banque.services;

import fr.gouv.finances.dgfip.banque.entities.Personne;

public interface PersonneServiceInterface {
    Personne creerPersonne(String nom, String prenom);
}
