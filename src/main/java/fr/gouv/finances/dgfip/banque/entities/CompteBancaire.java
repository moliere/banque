package fr.gouv.finances.dgfip.banque.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public abstract class CompteBancaire {
    private final static double LIM_MIN_COMPTE = -500.;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    public UUID getId() {
	return id;
    }

    public void setId(UUID id) {
	this.id = id;
    }

    // L'annotation "@ManyToOne" permet de créer la relation entre Compte
    // Bancaire et Personne
    @ManyToOne
    private Personne titulaire;

    // L'annotation "@ManyToOne" permet de créer la relation entre Compte
    // Bancaire et Banque
    @ManyToOne
    private Banque banque;

    private String codeGuichet;
    private String numCompte;
    private String cle;
    private Double solde;

    // Association forte des opérations au Compte Bancaire
    @OneToMany(cascade = { CascadeType.PERSIST,
	    CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true)
//    @OneToMany(cascade = { CascadeType.PERSIST,
//	    CascadeType.REMOVE }, orphanRemoval = true)
    private List<Operation> listeOperations = new ArrayList<Operation>();

    public CompteBancaire() {
	super();
    }

    public CompteBancaire(Banque maBanque, Personne titulaire,
	    String codeGuichet) {
	this.banque = maBanque;
	this.titulaire = titulaire;
	this.codeGuichet = codeGuichet;
	this.numCompte = maBanque.genererNumCompte();
	this.cle = this.genererCle();
	this.solde = 0.;
	this.listeOperations.add(new Operation(this, "SOLDE INITIAL", 0.));
    }

    public CompteBancaire(Banque maBanque, Personne titulaire,
	    String codeGuichet, Double solde) {
	this.banque = maBanque;
	this.titulaire = titulaire;
	this.codeGuichet = codeGuichet;
	this.numCompte = maBanque.genererNumCompte();
	this.cle = this.genererCle();
	this.solde = solde;
	this.listeOperations.add(new Operation(this, "SOLDE INITIAL", solde));
    }

    public String getRib() {
	return this.codeGuichet + " " + this.numCompte + " " + this.cle;

    }

    public String toString() {
	String affichage = new String();
	affichage = "\n" + "Synthèse du compte: " + this.banque.getCodeBanque()
		+ " " + this.codeGuichet + " " + this.numCompte + " " + this.cle
		+ "\n" + "Titulaire: " + this.titulaire + "\n"
		+ "+---------+-------------------------+-------------------------+-----------+\n"
		+ "| Num opé | Date opération          | Libellé                 | Montant   |\n"
		+ "+---------+-------------------------+-------------------------+-----------";

	for (Operation o : this.getListeOperations()) {
	    affichage = affichage + o.toString();
	}

	affichage = affichage + "\n" + "Solde: " + this.solde + "\n";

	return affichage;

    }

    public Banque getBanque() {
	return banque;
    }

    public void setBanque(Banque maBanque) {
	this.banque = maBanque;
    }

    public Personne getTitulaire() {
	return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
	this.titulaire = titulaire;
    }

    public String getCodeGuichet() {
	return codeGuichet;
    }

    public void setCodeGuichet(String codeGuichet) {
	this.codeGuichet = codeGuichet;
    }

    public String getNumCompte() {
	return numCompte;
    }

    public void setNumCompte(String numCompte) {
	this.numCompte = numCompte;
    }

    public String getCle() {
	return cle;
    }

    public void setCle(String cle) {
	this.cle = cle;
    }

    public Double getSolde() {
	return solde;
    }

    public void setSolde(Double solde) {
	this.solde = solde;
    }

    public static double getLimMinCompte() {
	return LIM_MIN_COMPTE;
    }

    public String genererCle() {
	return "000";
    }

    public List<Operation> getListeOperations() {
	return listeOperations;
    }

    public void setListeOperations(List<Operation> listeOperations) {
	this.listeOperations = listeOperations;
    }

}
