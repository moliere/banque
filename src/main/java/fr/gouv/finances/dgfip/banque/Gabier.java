package fr.gouv.finances.dgfip.banque;

import java.util.List;

import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.entities.Banque;

/**
 * 
 * @author stagiaire2
 *
 */
@Component
public class Gabier {

    private SystemeBancaireInterface systemeBancaire;

    /**
     * 
     * @param maBanque
     */
    public Gabier(SystemeBancaireInterface systemeBancaire) {
	this.setSystemeBancaire(systemeBancaire);
    }

    /**
     * 
     * @param numCarte
     * @param codePin
     * @return
     * @throws SystemeBancaireException
     */
    public List<String> accesComptes(Banque banque, String numCarte,
	    String codePin) throws SystemeBancaireException {
	if (this.systemeBancaire.verifierCodePin(banque, numCarte, codePin))
	    return this.systemeBancaire.rechercherRIBCompteCarte(banque,
		    numCarte);
	else {
	    throw new SystemeBancaireException("Code Pin Incorrect");
	}

    }

    /**
     * 
     * @param ribCompte
     * @param montant
     * @return
     * @throws SystemeBancaireException
     */
    public int retirerEspeces(Banque banque, String ribCompte, Double montant)
	    throws SystemeBancaireException {
	montant = 0 - montant;
	return (this.systemeBancaire.creerOpération(banque, ribCompte,
		"Retrait Gabier " + montant, montant));
    }

    /**
     * 
     * @return
     */
    public SystemeBancaireInterface getSystemeBancaire() {
	return this.systemeBancaire;
    }

    /**
     * 
     * @param maBanque
     */
    public void setSystemeBancaire(SystemeBancaireInterface systemeBancaire) {
	this.systemeBancaire = systemeBancaire;
    }

}
