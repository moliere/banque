package fr.gouv.finances.dgfip.banque.entities;

import javax.persistence.Entity;

@Entity
public class CompteCourant extends CompteBancaire {

    public CompteCourant() {
	super();
    }

    public CompteCourant(Banque maBanque, Personne titulaire,
	    String codeGuichet) {
	super(maBanque, titulaire, codeGuichet);
    }

    public CompteCourant(Banque maBanque, Personne titulaire,
	    String codeGuichet, Double soldeInitial) {
	super(maBanque, titulaire, codeGuichet, soldeInitial);
    }

    public String toString() {
	return super.toString();
    }

}
