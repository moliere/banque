package fr.gouv.finances.dgfip.banque.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.dao.BanqueDao;
import fr.gouv.finances.dgfip.banque.dao.CarteBancaireDao;
import fr.gouv.finances.dgfip.banque.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.entities.Banque;
import fr.gouv.finances.dgfip.banque.entities.CarteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteCourant;
import fr.gouv.finances.dgfip.banque.entities.CompteEpargne;
import fr.gouv.finances.dgfip.banque.entities.Personne;
import fr.gouv.finances.dgfip.banque.services.BanqueServiceInterface;

@Component
public class BanqueService implements BanqueServiceInterface {

    @Autowired
    private BanqueDao banquedao;

    @Autowired
    private CompteBancaireDao comptebancairedao;

    @Autowired
    private CarteBancaireDao cartebancairedao;

    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
	    String codeGuichet) {

	List<CompteBancaire> listeComptes = new ArrayList<CompteBancaire>();
	CompteCourant monCompte = new CompteCourant(banque, titulaire,
		codeGuichet);
	listeComptes = banque.getListeComptes();
	listeComptes.add(monCompte);
	banque.setListeComptes(listeComptes);
	comptebancairedao.save(monCompte);
	// banquedao.save(banque);
	return monCompte;
    }

    public CompteCourant creerCompteCourant(Banque banque, Personne titulaire,
	    String codeGuichet, Double soldeInitial) {

	List<CompteBancaire> listeComptes = new ArrayList<CompteBancaire>();
	CompteCourant monCompte = new CompteCourant(banque, titulaire,
		codeGuichet, soldeInitial);
	listeComptes = banque.getListeComptes();
	listeComptes.add(monCompte);
	banque.setListeComptes(listeComptes);
	comptebancairedao.save(monCompte);
	// banquedao.save(banque);
	return monCompte;
    }

    public CompteEpargne creerCompteEpargne(Banque banque, Personne titulaire,
	    String codeGuichet, Double soldeInitial, Double taux) {

	List<CompteBancaire> listeComptes = new ArrayList<CompteBancaire>();
	CompteEpargne monCompte = new CompteEpargne(banque, titulaire,
		codeGuichet, soldeInitial, taux);
	listeComptes = banque.getListeComptes();
	listeComptes.add(monCompte);
	banque.setListeComptes(listeComptes);
	comptebancairedao.save(monCompte);
	// banquedao.save(banque);
	return monCompte;
    }

    public CarteBancaire creerCarte(Banque banque, Personne titulaire,
	    CompteCourant compte) {

	List<CarteBancaire> listeCartes = new ArrayList<CarteBancaire>();

	CarteBancaire cb = null;
	CompteBancaire monCompte = null;

	for (CompteBancaire c : banque.getListeComptes()) {
	    if (c.getRib().equals(compte.getRib())) {
		monCompte = c;
		System.out.println(monCompte);
	    }
	}
	if (monCompte != null) {
	    cb = new CarteBancaire(titulaire, monCompte);

	    listeCartes = banque.getListeCartes();
	    listeCartes.add(cb);
	    banque.setListeCartes(listeCartes);
	    cartebancairedao.save(cb);
	}

	return cb;
    }

    public void afficherSyntheseComptes(Banque banque) {
	for (CompteBancaire c : banque.getListeComptes()) {
	    System.out.println(c);
	}
    }

    public void afficherSyntheseCartes(Banque banque) {
	for (CarteBancaire cb : banque.getListeCartes()) {
	    System.out.println(cb);
	}
    }

    public CarteBancaire lierCarte(CarteBancaire carte, CompteBancaire compte) {
	carte.setCompteBancaireCB(compte);
	return carte;
    }

    @Override
    public Banque creerBanque(String codeBanque) {
	Banque banque = banquedao.findByCodeBanque(codeBanque);
	if (banque == null) {
	    banque = new Banque(codeBanque);
	    banquedao.save(banque);
	}

	return banque;
    }

}
