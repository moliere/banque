package fr.gouv.finances.dgfip.banque.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.entities.Personne;

public interface PersonneDao extends CrudRepository<Personne, UUID> {

}
