package fr.gouv.finances.dgfip.banque.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.entities.Banque;

public interface BanqueDao extends CrudRepository<Banque, UUID> {
    Banque findByCodeBanque(String codeBanque);
}
