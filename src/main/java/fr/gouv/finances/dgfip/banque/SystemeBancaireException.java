package fr.gouv.finances.dgfip.banque;

public class SystemeBancaireException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -409319736938634710L;

    public SystemeBancaireException(String message) {
	super(message);
    }

}
