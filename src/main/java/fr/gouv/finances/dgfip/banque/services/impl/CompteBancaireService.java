package fr.gouv.finances.dgfip.banque.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.CompteException;
import fr.gouv.finances.dgfip.banque.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.dao.OperationDao;
import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteEpargne;
import fr.gouv.finances.dgfip.banque.entities.Operation;
import fr.gouv.finances.dgfip.banque.services.CompteBancaireServiceInterface;

@Component
public class CompteBancaireService implements CompteBancaireServiceInterface {

    @Autowired
    private CompteBancaireDao comptebancairedao;

    @Autowired
    private OperationDao operationdao;

    public Operation creerOperation(CompteBancaire compte, String libelle,
	    Double montant) throws CompteException {

	List<Operation> listeOperations = new ArrayList<Operation>();
	Operation op;

	System.out.println(
		"*********************************************************");
	System.out.println("Solde avant opération: " + compte.getSolde());
	System.out.println("Montant opération : " + montant);

	if ((compte.getSolde() + montant) < CompteBancaire.getLimMinCompte()) {
	    throw new CompteException(
		    "solde inuffisant (valeur après opération < "
			    + CompteBancaire.getLimMinCompte() + ")");
	} else {
	    op = new Operation(compte, libelle, montant);
	    listeOperations = compte.getListeOperations();
	    listeOperations.add(op);
	    compte.setListeOperations(listeOperations);
	    op.setCompte(compte);
	    operationdao.save(op);
	    compte.setSolde(compte.getSolde() + montant);
	    comptebancairedao.save(compte);
	    System.out.println("Solde après opération: " + compte.getSolde());
	}
	System.out.println(
		"*********************************************************\n");
	return op;
    }

    public void afficherSyntheseOperations(CompteBancaire compte) {

	System.out.println(comptebancairedao.findById(compte.getId()));
    }

    public Double calculerInteret(CompteEpargne compte) {
	return (compte.getSolde() * compte.getTxInteret());
    }

}
