package fr.gouv.finances.dgfip.banque.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.dao.PersonneDao;
import fr.gouv.finances.dgfip.banque.entities.Personne;
import fr.gouv.finances.dgfip.banque.services.PersonneServiceInterface;

@Component
public class PersonneService implements PersonneServiceInterface {

    @Autowired
    private PersonneDao personnedao;

    public Personne creerPersonne(String nom, String prenom) {

	Personne personne = new Personne(nom, prenom);
	personnedao.save(personne);
	return personne;
    }

}
