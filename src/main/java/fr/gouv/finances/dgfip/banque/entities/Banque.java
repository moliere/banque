package fr.gouv.finances.dgfip.banque.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Banque {

//    private static int numeroNextCompte = 1;
//    private static int numeroNextCarte = 1;
    @Transient
    private int numeroNextCompte = 1;
    @Transient
    private int numeroNextCarte = 1;

    // unique: each Commune must have a different name in DB
    // length: limit the size of DB field NOM to 60 char
    @Column(unique = true, length = 20)
    private String codeBanque;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    public UUID getId() {
	return id;
    }

    public void setId(UUID id) {
	this.id = id;
    }

    // Association forte des comptes à la banque
    @OneToMany(cascade = { CascadeType.PERSIST,
	    CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true)
//    @OneToMany(cascade = { CascadeType.PERSIST,
//	    CascadeType.REMOVE }, orphanRemoval = true)
    private List<CompteBancaire> listeComptes = new ArrayList<CompteBancaire>();

    // Association forte des cartes à la banque
    @OneToMany(cascade = { CascadeType.PERSIST,
	    CascadeType.REMOVE }, fetch = FetchType.EAGER, orphanRemoval = true)
//    @OneToMany(cascade = { CascadeType.PERSIST,
//	    CascadeType.REMOVE }, orphanRemoval = true)
    private List<CarteBancaire> listeCartes = new ArrayList<CarteBancaire>();

    public Banque() {
	super();
    }

    public Banque(String codeBanque) {
	super();
	this.codeBanque = codeBanque;
    }

    public String getCodeBanque() {
	return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
	this.codeBanque = codeBanque;
    }

    public List<CompteBancaire> getListeComptes() {
	return listeComptes;
    }

    public void setListeComptes(List<CompteBancaire> listeComptes) {
	this.listeComptes = listeComptes;
    }

    public List<CarteBancaire> getListeCartes() {
	return listeCartes;
    }

    public void setListeCartes(List<CarteBancaire> listeCartes) {
	this.listeCartes = listeCartes;
    }

    public String genererNumCompte() {
	return String.valueOf(numeroNextCompte++);
    }

    public String genererNumCarte() {
	return String.valueOf(numeroNextCarte++);
    }
}
