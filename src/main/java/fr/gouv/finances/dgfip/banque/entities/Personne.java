package fr.gouv.finances.dgfip.banque.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Personne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String nom;
    private String prenom;

    public Personne() {
	super();
    }

    public Personne(String nom, String prenom) {
	super();
	this.nom = nom;
	this.prenom = prenom;
    }

    @Override
    public String toString() {
	return this.getNom() + " " + this.getPrenom() + " ("
		+ this.getClass().getSimpleName() + ")";
    }

    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    public String getPrenom() {
	return prenom;
    }

    public void setPrenom(String prenom) {
	this.prenom = prenom;
    }
}
