package fr.gouv.finances.dgfip.banque.services;

import fr.gouv.finances.dgfip.banque.CompteException;
import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteEpargne;
import fr.gouv.finances.dgfip.banque.entities.Operation;

public interface CompteBancaireServiceInterface {

    Operation creerOperation(CompteBancaire compte, String libelle,
	    Double montant) throws CompteException;

    void afficherSyntheseOperations(CompteBancaire compte);

    Double calculerInteret(CompteEpargne compte);
}
