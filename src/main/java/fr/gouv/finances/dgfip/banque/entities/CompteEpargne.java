package fr.gouv.finances.dgfip.banque.entities;

import javax.persistence.Entity;

@Entity
public class CompteEpargne extends CompteBancaire {

    protected Double txInteret;

    public CompteEpargne() {
	super();
    }

    public CompteEpargne(Banque maBanque, Personne titulaire,
	    String codeGuichet, Double soldeInitial, Double txInteret) {
	super(maBanque, titulaire, codeGuichet, soldeInitial);
	this.txInteret = txInteret;
    }

    public Double getTxInteret() {
	return txInteret;
    }

    public void setTxInteret(Double txInteret) {
	this.txInteret = txInteret;
    }

}
