package fr.gouv.finances.dgfip.banque.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.entities.Banque;
import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;

public interface CompteBancaireDao
	extends CrudRepository<CompteBancaire, UUID> {

    // @Query("SELECT c FROM CompteBancaire c WHERE c.totoBanque = :banque")
    Iterable<CompteBancaire> findByBanque(Banque banque);
}
