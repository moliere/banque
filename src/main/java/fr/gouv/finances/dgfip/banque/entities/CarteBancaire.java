package fr.gouv.finances.dgfip.banque.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CarteBancaire {

    private final static int NBMOISVALIDITECARTE = 18;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(length = 4)
    private String codePin;

    private String numCarte;
    private Date dateExpiration;

    // L'annotation "@ManyToOne" permet de créer la relation entre CarteBancaire
    // et CompteBancaire
    @ManyToOne
    private CompteBancaire compteBancaireCB;

    // L'annotation "@ManyToOne" permet de créer la relation entre CarteBancaire
    // et Personne
    @ManyToOne
    private Personne titulaire;
    @ManyToOne
    private Banque banque;

    public CarteBancaire() {
	super();
    }

    public CarteBancaire(Personne titulaire, CompteBancaire compteCourantCB) {
	this.compteBancaireCB = compteCourantCB;
	this.banque = this.compteBancaireCB.getBanque();
	this.titulaire = titulaire;
	this.codePin = "0000";
	this.numCarte = compteCourantCB.getBanque().genererNumCarte();

	Calendar calendar = Calendar.getInstance();
	calendar.add(Calendar.MONTH, NBMOISVALIDITECARTE);
	this.dateExpiration = calendar.getTime();
    }

    public String toString() {
	return this.compteBancaireCB
		+ "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
		+ "Numéro Carte : " + this.numCarte + " ( Expire le "
		+ dateExpiration + ")\n"
		+ "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";

    }

    public CompteBancaire getCompteBancaireCB() {
	return compteBancaireCB;
    }

    public void setCompteBancaireCB(CompteBancaire compteBancaireCB) {
	this.compteBancaireCB = compteBancaireCB;
    }

    public Personne getTitulaire() {
	return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
	this.titulaire = titulaire;
    }

    public String getCodePin() {
	return codePin;
    }

    public void setCodePin(String codePin) {
	this.codePin = codePin;
    }

    public String getNumCarte() {
	return numCarte;
    }

    public void setNumCarte(String numCarte) {
	this.numCarte = numCarte;
    }

    public Date getDateExpiration() {
	return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
	this.dateExpiration = dateExpiration;
    }

    public boolean verifierPin(String codePin) {
	return (this.codePin == codePin);
    }

}
