package fr.gouv.finances.dgfip.banque;

import java.util.List;

import fr.gouv.finances.dgfip.banque.entities.Banque;

public interface SystemeBancaireInterface {
    List<String> rechercherRIBCompteCarte(Banque banque, String numCarte)
	    throws SystemeBancaireException;

    int creerOpération(Banque banque, String ribCompte, String libelle,
	    Double montant) throws SystemeBancaireException;

    Boolean verifierCodePin(Banque banque, String numCarte, String codePin)
	    throws SystemeBancaireException;
}
