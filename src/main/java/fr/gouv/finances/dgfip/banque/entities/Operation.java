package fr.gouv.finances.dgfip.banque.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Operation {

    private static int numeroNextOperation = 1;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private int numOperation;
    private Date dateOperation;
    private String libelle;
    private Double montant;

    @ManyToOne
    private CompteBancaire compte;

    public Operation() {
	super();
    }

    public Operation(CompteBancaire compte, String libelle, Double montant) {
	this.compte = compte;
	this.numOperation = genererNumOperation();
	this.dateOperation = new Date();
	this.libelle = libelle;
	this.montant = montant;
    }

    /*
     * public Operation(String libelle, Double montant) { this.numOperation =
     * genererNumOperation(); this.dateOperation = new Date(); this.libelle =
     * libelle; this.montant = montant; }
     */

    @Override
    public String toString() {
	String paddedNumOperation = String.format("%7d", this.numOperation);
	String paddedDate = String.format("%-23s",
		new SimpleDateFormat("yyyy-MM-dd").format(this.dateOperation)
			+ " T " + new SimpleDateFormat("HH:mm:ss")
				.format(this.dateOperation));
	String paddedLibelle = String.format("%-23s", this.libelle);
	String paddedMontant = String.format("%10s", this.montant);

	return "\n| " + paddedNumOperation + " | " + paddedDate + " | "
		+ paddedLibelle + " | " + paddedMontant + "|\n"
		+ "+---------+-------------------------+-------------------------+------------";

    }

    public int getNumOperation() {
	return numOperation;
    }

    public void setNumOperation(int numOperation) {
	this.numOperation = numOperation;
    }

    public Date getDateOperation() {
	return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
	this.dateOperation = dateOperation;
    }

    public String getLibelle() {
	return libelle;
    }

    public void setLibelle(String libelle) {
	this.libelle = libelle;
    }

    public Double getMontant() {
	return montant;
    }

    public void setMontant(Double montant) {
	this.montant = montant;
    }

    public int genererNumOperation() {
	return Operation.numeroNextOperation++;
    }

    public CompteBancaire getCompte() {
	return compte;
    }

    public void setCompte(CompteBancaire compte) {
	this.compte = compte;
    }

}
