package fr.gouv.finances.dgfip.banque.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.CompteException;
import fr.gouv.finances.dgfip.banque.SystemeBancaireException;
import fr.gouv.finances.dgfip.banque.SystemeBancaireInterface;
import fr.gouv.finances.dgfip.banque.entities.Banque;
import fr.gouv.finances.dgfip.banque.entities.CarteBancaire;
import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;
import fr.gouv.finances.dgfip.banque.entities.Operation;

@Component
public class SystemeBancaire implements SystemeBancaireInterface {

    @Autowired
    private CompteBancaireService compteBancaireService;

    public List<String> rechercherRIBCompteCarte(Banque banque, String numCarte)
	    throws SystemeBancaireException {

	List<String> listeRib = new ArrayList<String>();
	for (CarteBancaire cb : banque.getListeCartes()) {
	    if (cb.getNumCarte().equals(numCarte)) {
		listeRib.add(cb.getCompteBancaireCB().getRib());
	    }
	}
	if (listeRib.isEmpty()) {
	    throw new SystemeBancaireException("Aucun RIB attaché à ce compte");
	}

	return listeRib;
    }

    public int creerOpération(Banque banque, String ribCompte, String libelle,
	    Double montant) throws SystemeBancaireException {

	Operation operation;

	for (CompteBancaire c : banque.getListeComptes()) {

	    if (c.getRib().equals(ribCompte)) {
		try {
		    operation = compteBancaireService.creerOperation(c, libelle,
			    montant);

		    return operation.getNumOperation();
		} catch (CompteException e) {
		    throw new SystemeBancaireException(
			    e.getMessage() + " / Opération annulée");
		}
	    }
	}

	throw new SystemeBancaireException("Aucun RIB attaché à ce compte");
    }

    public Boolean verifierCodePin(Banque banque, String numCarte,
	    String codePin) throws SystemeBancaireException {
	for (CarteBancaire cb : banque.getListeCartes()) {
	    if (cb.getNumCarte().equals(numCarte)) {
		return cb.verifierPin(codePin);
	    }
	}

	throw new SystemeBancaireException("Carte non trouvée");

    }

}
