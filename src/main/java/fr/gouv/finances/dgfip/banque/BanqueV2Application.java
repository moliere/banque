package fr.gouv.finances.dgfip.banque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanqueV2Application {
    public static void main(String[] args) {
        SpringApplication.run(BanqueV2Application.class, args);
    }
}
