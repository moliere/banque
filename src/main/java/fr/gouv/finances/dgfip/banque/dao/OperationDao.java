package fr.gouv.finances.dgfip.banque.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.entities.CompteBancaire;
import fr.gouv.finances.dgfip.banque.entities.Operation;

public interface OperationDao extends CrudRepository<Operation, UUID> {
    Iterable<Operation> findByCompte(CompteBancaire compte);
}
