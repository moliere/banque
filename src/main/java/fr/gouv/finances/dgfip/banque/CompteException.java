package fr.gouv.finances.dgfip.banque;

public class CompteException extends Exception {

    private static final long serialVersionUID = 7002618186465271814L;

    public CompteException(String message) {
	super(message);
    }

}
