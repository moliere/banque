package fr.gouv.finances.dgfip.banque.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BanqueTests {

    @Test
    void testCreerBanque() {
	Banque banque = new Banque("TEST");
	assertTrue(banque.getListeComptes().isEmpty());
	assertTrue(banque.getListeCartes().isEmpty());
	assertEquals(banque.getCodeBanque(), "TEST");
//	assertEquals(banque.getNumCompte(), 0);
    }

}
