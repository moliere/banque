package fr.gouv.finances.dgfip.banque.services;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.gouv.finances.dgfip.banque.entities.Banque;
import fr.gouv.finances.dgfip.banque.entities.CompteCourant;
import fr.gouv.finances.dgfip.banque.entities.Personne;

@SpringBootTest
class BanqueServiceInterfaceTests {

    @Autowired
    private BanqueServiceInterface serviceBancaire;

    @Test
    void creerCompteCourant_simple() {
	CompteCourant cc = serviceBancaire.creerCompteCourant(
		new Banque("TEST"), new Personne("nom", "prenom"), "0000");
	assertTrue(cc.getSolde() == 0);
	assertTrue(cc.getCodeGuichet().equals("0000"));
	assertTrue(cc.getCle().equals("000"));
	assertTrue(cc.getRib().equals("0000 1 000"));
    }

    @Test
    void creerCompteCourant_extreme1() {
	CompteCourant cc = serviceBancaire.creerCompteCourant(new Banque(""),
		new Personne("nom", "prenom"), "0000");
	assertTrue(cc.getSolde() == 0);
	assertTrue(cc.getCodeGuichet().equals("0000"));
	assertTrue(cc.getCle().equals("000"));
	System.out.println(cc.getRib());
	assertTrue(cc.getRib().equals("0000 2 000"));
    }

    @Test
    void creerCompteCourant_extreme2() {
	CompteCourant cc = serviceBancaire.creerCompteCourant(new Banque("b"),
		new Personne("", "prenom"), "0000");
	assertTrue(cc.getSolde() == 0);
	assertTrue(cc.getCodeGuichet().equals("0000"));
	assertTrue(cc.getCle().equals("000"));
	System.out.println(cc.getRib());
	assertTrue(cc.getRib().equals("0000 3 000"));
    }

    @Test
    void creerCompteCourant_extreme3() {
	CompteCourant cc = serviceBancaire.creerCompteCourant(new Banque("b"),
		new Personne("nom", ""), "0000");
	assertTrue(cc.getSolde() == 0);
	assertTrue(cc.getCodeGuichet().equals("0000"));
	assertTrue(cc.getCle().equals("000"));
	System.out.println(cc.getRib());
	assertTrue(cc.getRib().equals("0000 4 000"));
    }

    @Test
    void creerCompteCourant_extreme4() {
	CompteCourant cc = serviceBancaire.creerCompteCourant(new Banque("b"),
		new Personne("nom", "prenom"), "");
	assertTrue(cc.getSolde() == 0);
	assertTrue(cc.getCodeGuichet().equals(""));
	assertTrue(cc.getCle().equals("000"));
	System.out.println(cc.getRib());
	assertTrue(cc.getRib().equals(" 5 000"));
    }
}
